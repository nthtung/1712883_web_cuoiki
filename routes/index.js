const express = require('express');
const router = express.Router();

const Product = require('../models/product');
// require controller modules

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Vegefoods' });
});

router.get('/index.html', (req,res) => {
  res.render('index', { title: 'Vegefoods' });
});

router.get('/about.html',(req,res) => {
  res.render('about',{title: 'About Us'});
});

router.get('/shop.html',async (req,res) => {
  await Product.find({}, (err, products) => {
    if (err) throw err;
    res.render('shop', { title: 'Shop', products: products});
  });
});

router.get('/shop/:id', async(req,res)=> {
  const id = req.params.id;
  await Product.findOne({_id: id}, (err, product) => {
    if (err) throw err;
    res.render('product-single', {title: product.name, product: product.toObject()});
  });
});

router.get('/shop', async(req,res) => {
  const type = req.query.type;
  if (type == 'all') {
    await Product.find({}, (err, products) => {
      if (err) throw err;
      res.render('shop', { title: 'Shop', products: products});
    });
  } else {
    await Product.find({type: type}, (err, products) => {
      if (err) throw err;
      res.render('shop', { title: 'Shop', products: products});
    });
  }
});


router.get('/contact.html',(req,res) => {
  res.render('contact',{title: 'Contact'});
});

module.exports = router;
