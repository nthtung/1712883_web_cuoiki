const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const ProductSchema = new Schema({
  name: {
    type: String,
    required: true
  },
  image: {
    type: String,
    required: true
  },
  rating: {
    type: String,
    required: true
  },
  sold: {
    type: Number,
    required: true
  },
  detail: {
    type: String,
    required: true
  },
  size: {
    type: Array,
    required: true
  },
  available: {
    type: Number,
    required: true
  },
  type: {
    type: Number,
    required: true
  }
});

// Export module
module.exports = mongoose.model('Product', ProductSchema, 'product');